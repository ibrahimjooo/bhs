<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Data Employee</h3>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <button class="btn btn-success" onclick="tampilkan()">Tambah Data</button>
        <br>
        <table id="example2" class="table table-bordered table-hover">
          <thead>
          <tr>
            <th width="5%">ID</th>
            <th>Nama Lengkap</th>
            <th>Tempat Lahir</th>
            <th>Tanggal Lahir</th>
            <th>Gaji Pokok</th>
            <th>Tunjangan</th>
            <th>Action</th>
          </tr>
          </thead>
          <tbody>
        <?php 
        setlocale(LC_MONETARY,"en_US");
        include $_SERVER['DOCUMENT_ROOT']."/behaustexnative/Library/koneksi.php";
        $hasil = $lokal->query("SELECT *, DATE_FORMAT(BirthDate, '%d %M %Y') as zika FROM employee");
        while ($row = mysqli_fetch_assoc($hasil)) 
        {
          echo '<tr>
                  <td align="center">'.$row['EmployeeID'].'</td>
                  <td>'.$row['Description'].'</td>
                  <td align="center">'.$row['BirthPlace'].'</td>
                  <td align="center">'.$row['zika'].'</td>
                  <td align="right">Rp. '.number_format($row['Sallary'], 2).'</td>
                  <td align="right">Rp. '.number_format($row['Tunjangan'], 2).'</td>
                  <td align="center"><button class="btn btn-warning" onclick="edit('.$row['EmployeeID'].')">Edit Data</button></td>
                </tr>';
        }
        
        ?>            
        </table>
      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->


    <!-- /.card -->
  </div>
  <!-- /.col -->
</div>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Input Data Employee</h4>
      </div>
      <div class="modal-body">
      <div class="modal-body">
        <form id="bts">
          <div class="row">
            <div class="col-md-4">
              <label>Nama Lengkap</label>
            </div>
            <div class="col-md-8">
              <input type="hidden" name="action" id="action">
              <input type="hidden" name="EmployeeID" id="EmployeeID">
              <input type="text" name="nama" id="Description" class="form-control">
            </div>
            <div class="col-md-4">
              <label>Tempat Lahir</label>
            </div>
            <div class="col-md-8">
              <input type="text" name="tempat" id="BirthPlace" class="form-control">
            </div>
            <div class="col-md-4">
              <label>Tanggal Lahir</label>
            </div>
            <div class="col-md-8">
              <input type="date" name="tanggal" id="BirthDate" class="form-control">
            </div>    
            <div class="col-md-4">
              <label>Jabatan</label>
            </div>
            <div class="col-md-8">
              <input type="text" name="rank" id="Rank" class="form-control">
            </div> 
            <div class="col-md-4">
              <label>Gaji Pokok</label>
            </div>
            <div class="col-md-8">
              <input type="text" name="gapok" id="Sallary" class="form-control">
            </div> 
            <div class="col-md-4">
              <label>Tunjangan</label>
            </div>
            <div class="col-md-8">
              <input type="text" name="tunjangan" id="Tunjangan" class="form-control">
            </div>                                          
            <div class="col-md-4">
              <label>BPJS</label>
            </div>
            <div class="col-md-8">
              <select class="form-control" name="bpjs" id="bpjs">
                <option>Ya</option>
                <option>Tidak</option>
              </select>
            </div>                       
          </div>
        </form>
      </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" onclick="simpan()">Simpan</button>&nbsp;&nbsp;&nbsp;
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>