<?php 
include $_SERVER['DOCUMENT_ROOT']."/behaustexnative/Library/koneksi.php";
$uhuk = $_POST['guna'];
$workdays = array();
$type = CAL_GREGORIAN;
$month = $uhuk; 
$year = date('Y'); 
$day_count = cal_days_in_month($type, $month, $year); 

for ($i = 1; $i <= $day_count; $i++) 
{
        $date = $year.'/'.$month.'/'.$i; 
        $get_name = date('l', strtotime($date)); 
        $day_name = substr($get_name, 0, 3); 

        if($day_name != 'Sun' && $day_name != 'Sat')
        {
            $workdays[] = $i;
        }
}

$html = '';
$html .= '<table id="example3" class="table table-bordered table-hover">
	          <thead>
		          <tr>
		            <th width="5%">ID</th>
		            <th>Nama Lengkap</th>
		            <th>Gaji Pokok</th>
		            <th>Tunjangan</th>
		            <th>BPJS</th>
		            <th>Action</th>
		            <th>NWNP</th>
		            <th>Actual Lembur</th>
		            <th width="15%">Gaji Bulan Ini</th>
		          </tr>
	          </thead>
	          <tbody>';
	        setlocale(LC_MONETARY,"en_US");
	        $no = 1;
	        $hasil = $lokal->query("SELECT *, DATE_FORMAT(BirthDate, '%d %M %Y') as zika FROM employee");
	        while ($row = mysqli_fetch_assoc($hasil)) 
	        {
	          $html .= '<tr>
	                  <td align="center">'.$row['EmployeeID'].'</td>
	                  <td>'.$row['Description'].'</td>
	                  <td align="right">Rp. '.number_format($row['Sallary'], 2).'</td>
	                  <td align="right">Rp. '.number_format($row['Tunjangan'], 2).'</td>
	                  <td align="center">'.$row['Bpjs'].'</td>
	                  <td align="center"><button class="btn btn-warning" onclick="proses('.$row['EmployeeID'].','.$uhuk.','.$no.')">Ambil Data</button></td>	                  
	                  <td align="center" id="work'.$no.'">0</td>
	                  <td align="center" id="lembur'.$no.'">0</td>
	                  <td align="center" id="total'.$no.'">0</td>
	                </tr>';
	             $no++;
	        }	
	       
	$html .= '</tbody></table>';  

$arrayName = array('status' => 'sukses', 'iki' => $html );        
echo json_encode($arrayName);
?>