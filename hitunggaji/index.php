<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>BHS</title>
  <?php include $_SERVER['DOCUMENT_ROOT']."/behaustexnative/Home/css.php"; ?>
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">
  <div class="preloader flex-column justify-content-center align-items-center">
    <img class="animation__shake" src="/behaustexnative/Library/dist/img/AdminLTELogo.png" alt="AdminLTELogo" height="60" width="60">
  </div>

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="/behaustexnative/Library/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">BHS</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar Menu -->
      <?php include $_SERVER['DOCUMENT_ROOT']."/behaustexnative/Home/menus.php" ?>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li><a href="/behaustexnative/Home/logout.php">Logout</a></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
      	<?php include $_SERVER['DOCUMENT_ROOT']."/behaustexnative/hitunggaji/data.php" ?>
        <!-- /.row -->
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2021 IbrahimJooo.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 3.1.0
    </div>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- /behaustexnative/wrapper -->


<?php include $_SERVER['DOCUMENT_ROOT']."/behaustexnative/Home/js.php"; ?>
</body>
</html>
<script type="text/javascript">
function tampilkan()
{
    var guna = $("#pilihan").val();
    var icis = {guna : guna};
    $.ajax({
      type : 'POST',
      data : icis,
      url: "showawal.php",
      cache: false,
      dataType: "json",
      success: function(data)
      {
        if(data.status === 'sukses')
        {
          $("#ikilobos").html(data.iki);
          $('#example3').DataTable( {
              "paging":   false,
              "ordering": false,
              "info":     false
          } );
        }
      }
    }); 
}

function simpan()
{
	var ikehcuk = $("#action").val()
	if(ikehcuk === 'update')
	{
		$.ajax({
		  type : 'POST',
		  data : $('#bts').serialize(),
		  url: "edithasil.php",
		  cache: false,
		  dataType: "json",
		  success: function(data)
		  {
		  	if(data.status === 'sukses')
		  	{
		  		alert("Data Berhasil Dirubah");
		  		location.reload();
		  	}
		  }
		});		
	}
	else
	{
		$.ajax({
		  type : 'POST',
		  data : $('#bts').serialize(),
		  url: "simpan.php",
		  cache: false,
		  dataType: "json",
		  success: function(data)
		  {
		  	if(data.status === 'sukses')
		  	{
		  		alert("Data Berhasil Disimpan");
		  		location.reload();
		  	}
		  }
		});		
	}
}

function proses(id, uhuk, urut)
{
	var data = {id : id, uhuk : uhuk, urut : urut};
	$.ajax({
	  type : 'POST',
	  data : data,
	  url: "ambilapi.php",
	  cache: false,
	  dataType: "json",
	  success: function(data)
	  {
	  	if(data.status === 'sukses')
	  	{
	  		$("#work"+data.urut).text(data.jamkerja);
	  		$("#lembur"+data.urut).text(data.lembur);
	  		$("#total"+data.urut).text(data.gajibulanini);
	  		
	  	}
	  }
	});	
}
</script>