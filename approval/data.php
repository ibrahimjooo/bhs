<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Hitung Gaji</h3>
      </div>
      <div class="card-body">
      	<div class="row">
      		<div class="col-md-4">
      			<span>Pilih Periode</span>
      		</div>
      		<div class="col-md-8">
      			<select name="pilihan" id="pilihan" class="form-control">
      				<option value="5">Mei 2021</option>
      				<option value="6">Juni 2021</option>
      				<option value="7">Juli 2021</option>
      			</select>
      		</div>
      	</div>
        <button class="btn btn-success" onclick="tampilkan()">Tampilkan</button>
        <br>
        <div id="ikilobos">
        	
        </div>
      </div>
    </div>
  </div>
</div>