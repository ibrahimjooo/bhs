<?php
include $_SERVER['DOCUMENT_ROOT']."/behaustexnative/Library/koneksi.php";
include_once('tcpdf/tcpdf.php');
$emp = $_GET['emp'];
$bulan = $_GET['bulan'];
$pagelayout = array(6, 5); //  or array($height, $width) 
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, 'in', $pagelayout, true, 'UTF-8', false);

$pdf->SetTitle('Gaji Bulanan');

$pdf->setFont('dejavusans', '', 12, '', true);
$pdf->SetPrintHeader(false);
$pdf->SetPrintFooter(false);
$pdf->AddPage();

$ikehmas = $lokal->query("SELECT * FROM employee WHERE EmployeeID = ".$emp);
$jossgan = mysqli_fetch_assoc($ikehmas);

$monthNum  = $bulan;
$dateObj   = DateTime::createFromFormat('!m', $monthNum);
$monthName = $dateObj->format('F'); // March

$jamkerja = $lokal->query("SELECT
			COUNT(*) as id
		FROM
		  workabsent WHERE EmployeeID = ".$jossgan['EmployeeID']." AND DATE_FORMAT(TransDate, '%m') = $bulan AND DAYOFWEEK(TransDate) IN (1,2,3,4,5)
		");
$hasiljamkerja = mysqli_fetch_assoc($jamkerja);

$jamlembur = $lokal->query("SELECT
	CASE WHEN Overtime IS NULL THEN 0 ELSE Overtime END as hasil
FROM
  overtime WHERE EmployeeID = ".$jossgan['EmployeeID']." AND DATE_FORMAT(TransDate, '%m') = $bulan
");
$hasilembur = mysqli_fetch_assoc($jamlembur);	
$gajibulanini = 0;
$kerja = (($hasiljamkerja['id'] > 0) ? $hasiljamkerja['id']*$jossgan['Sallary']/30 : 0 );
$lembur = (($hasilembur['hasil'] > 0) ? (($hasilembur['hasil'] > 4) ? $jossgan['Sallary']/173*($hasilembur['hasil']*2) : $jossgan['Sallary']/173*($hasilembur['hasil']*1)) : 0 );
$bpjs = (($jossgan['Bpjs'] === 'Ya') ? ($jossgan['Sallary']+$jossgan['Tunjangan'])*0.03 : 0); 
$gajibulanini = ($jossgan['Sallary']+$jossgan['Tunjangan']+$lembur)-$kerja-$bpjs;

$html = '';
$html .= '
<h1 style="text-align: center">PT. Mekar Jaya</h1>
<table width="100%">
<tr>
	<td width="40%">Nama</td>
	<td width="2%">:</td>
	<td width="58%">'.$jossgan['Description'].'</td>	
</tr>
<tr>
	<td width="40%">Jabatan</td>
	<td width="2%">:</td>
	<td width="58%">'.$jossgan['Rank'].'</td>		
</tr>
<tr>
	<td width="40%">Periode</td>
	<td width="2%">:</td>
	<td width="58%">'.$monthName.' - '.date('Y').'</td>		
</tr>
</table>
<br>
<table>
<tr>
	<td width="5%">A.</td>
	<td width="95%">Upah Tetap</td>
</tr>
<tr>
	<td width="5%"></td>
	<td width="40%">Gaji Pokok</td>
	<td width="5%">:</td>
	<td width="50%" align="right">Rp. '.number_format($jossgan['Sallary'], 2).'</td>
</tr>
<tr>
	<td width="5%"></td>
	<td width="40%">Tunjangan</td>
	<td width="5%">:</td>
	<td width="50%" align="right">Rp. '.number_format($jossgan['Tunjangan'], 2).'</td>
</tr>
<tr>
	<td width="5%">B.</td>
	<td width="95%">Upah Tidak Tetap</td>
</tr>
<tr>
	<td width="5%"></td>
	<td width="40%">Lembur</td>
	<td width="5%">:</td>
	<td width="50%" align="right">Rp. '.number_format($lembur, 2).'</td>
</tr>
<tr>
	<td width="5%">C.</td>
	<td width="95%">Potongan</td>
</tr>
<tr>
	<td width="5%"></td>
	<td width="40%">BPJS</td>
	<td width="5%">:</td>
	<td width="50%" align="right">Rp. '.number_format($bpjs, 2).'</td>
</tr>
<tr>
	<td width="5%"></td>
	<td width="40%">NWNP</td>
	<td width="5%">:</td>
	<td width="50%" align="right">Rp. '.number_format($kerja, 2).'</td>
</tr>
</table>
<br>
<table width="100%">
<tr>
	<td width="45%">Total Penerimaan</td>
	<td width="5%">:</td>
	<td width="50%" align="right">Rp. '.number_format($gajibulanini, 2).'</td>
</tr>
</table>';

$pdf->writeHTML($html);

$pdf->Output('test.pdf', 'I');
?>
