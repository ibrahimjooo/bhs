<?php 
include $_SERVER['DOCUMENT_ROOT']."/behaustexnative/Library/koneksi.php";
$uhuk = $_POST['guna'];

$html = '';
$html .= '<table id="example3" class="table table-bordered table-hover">
	          <thead>
		          <tr>
		            <th width="5%">ID</th>
		            <th>Nama Lengkap</th>
		            <th>Gaji Pokok</th>
		            <th>Tunjangan</th>
		            <th>BPJS</th>
		            <th>NWNP</th>
		            <th>Actual Lembur</th>
		            <th width="15%">Gaji Bulan Ini</th>
		            <th>Action</th>
		          </tr>
	          </thead>
	          <tbody>';
	        $no = 1;
	        $hasil = $lokal->query("SELECT *, DATE_FORMAT(BirthDate, '%d %M %Y') as zika FROM employee");
	        while ($row = mysqli_fetch_assoc($hasil)) 
	        {
	           $jamkerja = $lokal->query("SELECT
							COUNT(*) as id
						FROM
						  workabsent WHERE EmployeeID = ".$row['EmployeeID']." AND DATE_FORMAT(TransDate, '%m') = $uhuk AND DAYOFWEEK(TransDate) IN (1,2,3,4,5)
						");
				$hasiljamkerja = mysqli_fetch_assoc($jamkerja);

				$jamlembur = $lokal->query("SELECT
					CASE WHEN Overtime IS NULL THEN 0 ELSE Overtime END as hasil
				FROM
				  overtime WHERE EmployeeID = ".$row['EmployeeID']." AND DATE_FORMAT(TransDate, '%m') = $uhuk
				");
				$hasilembur = mysqli_fetch_assoc($jamlembur);	
				$gajibulanini = 0;
				$kerja = (($hasiljamkerja['id'] > 0) ? $hasiljamkerja['id']*$row['Sallary']/30 : 0 );
				$lembur = (($hasilembur['hasil'] > 0) ? (($hasilembur['hasil'] > 4) ? $row['Sallary']/173*($hasilembur['hasil']*2) : $row['Sallary']/173*($hasilembur['hasil']*1)) : 0 );
				$bpjs = (($row['Bpjs'] === 'Ya') ? ($row['Sallary']+$row['Tunjangan'])*0.03 : 0); 
				$gajibulanini = ($row['Sallary']+$row['Tunjangan']+$lembur)-$kerja-$bpjs;

				$cekapprv = $lokal->query("SELECT count(*) as ikeh
				FROM
				  sallaryrecap WHERE EmployeeID = ".$row['EmployeeID']." AND Bulan = $uhuk
				");
				$hasilaprv = mysqli_fetch_assoc($cekapprv);					
	          $html .= '<tr>
	                  <td align="center">'.$row['EmployeeID'].'</td>
	                  <td>'.$row['Description'].'</td>
	                  <td align="right">Rp. '.number_format($row['Sallary'], 2).'</td>
	                  <td align="right">Rp. '.number_format($row['Tunjangan'], 2).'</td>
	                  <td align="center">'.$row['Bpjs'].'</td>	                  
	                  <td align="center" id="work'.$no.'">'.$hasiljamkerja['id'].'</td>
	                  <td align="center" id="lembur'.$no.'">'.(empty($hasilembur['hasil']) ? 0 : $hasilembur['hasil']).'</td>
	                  <td align="center" id="total'.$no.'">Rp.'.number_format($gajibulanini,2).'</td>
	                  <td align="center"><button class="btn btn-info" id="appv'.$no.'" onclick="approve('.$no.','.$row['EmployeeID'].','.$uhuk.','.$kerja.','.$lembur.','.$gajibulanini.','.$bpjs.')" '.(($hasilaprv['ikeh'] > 0 ) ? 'disabled="disabled"' : '').'>Approve</button>&nbsp;&nbsp;&nbsp;<button class="btn btn-success"  id="cetak'.$no.'"onclick="cetak('.$no.','.$row['EmployeeID'].','.$uhuk.')" '.(($hasilaprv['ikeh'] > 0 ) ? '' : 'disabled="disabled"').'>Cetak</button></td>
	                </tr>';
	             $no++;
	        }	
	       
	$html .= '</tbody></table>';  

$arrayName = array('status' => 'sukses', 'iki' => $html );        
echo json_encode($arrayName);
?>