<?php 
session_start();
$emp = $_SESSION['ID'];

if($emp == 1)
{
  echo '<nav class="mt-2">
          <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <li class="nav-item">
              <a href="/behaustexnative/Home/index.php" class="nav-link">
                <i class="nav-icon far fas fa-tachometer-alt"></i>
                <p>
                  Dashboard
                </p>
              </a>
            </li>          
            <li class="nav-item">
              <a href="/behaustexnative/employee/index.php" class="nav-link">
                <i class="nav-icon far fa-image"></i>
                <p>
                  Employee
                </p>
              </a>
            </li>
            <li class="nav-item">
              <a href="/behaustexnative/hitunggaji/index.php" class="nav-link">
                <i class="nav-icon far fa-image"></i>
                <p>
                  Hitung Gaji
                </p>
              </a>
            </li>                       
          </ul>
        </nav>';
}
else
{
    echo '<nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
              <li class="nav-item">
                <a href="/behaustexnative/Home/index.php" class="nav-link">
                  <i class="nav-icon far fas fa-tachometer-alt"></i>
                  <p>
                    Dashboard
                  </p>
                </a>
              </li>           
              <li class="nav-item">
                <a href="/behaustexnative/approval/index.php" class="nav-link">
                  <i class="nav-icon far fa-image"></i>
                  <p>
                    Approval Gaji
                  </p>
                </a>
              </li>                    
            </ul>
          </nav>';
}
?>