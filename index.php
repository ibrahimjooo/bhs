<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>BHS</title>
  <?php include $_SERVER['DOCUMENT_ROOT']."/behaustexnative/Home/css.php"; ?>
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <!-- /.login-logo -->
  <div class="card card-outline card-primary">
    <div class="card-header text-center">
      <a class="h1"><b>BHS</a>
    </div>
    <div class="card-body">
      <p class="login-box-msg">Silahkan Login</p>

      <form id="loginkan">
        <div class="input-group mb-3">
          <input type="text" class="form-control" name="user" placeholder="Username">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control" name="pass" placeholder="Password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        </form>
        <div class="row">
          <!-- /.col -->
          <div class="col-4">
            <button type="button" onclick="loginkan()" class="btn btn-primary btn-block">Login</button>
          </div>
          <!-- /.col -->
        </div>
    </div>
  </div>
</div>
<?php include $_SERVER['DOCUMENT_ROOT']."/behaustexnative/Home/js.php"; ?>
</body>
</html>
<script type="text/javascript">
function loginkan()
{
      $.ajax({
          url:'loginkan.php',
          type:'POST',
          data: $('#loginkan').serialize(),
          success:function(response){
              var msg = "";
              if(response == 1){
                  window.location = "/behaustexnative/Home/index.php";
              }else{
                  msg = "Invalid username and password!";
                  alert(msg);
              }
          }
      });  
}
</script>
