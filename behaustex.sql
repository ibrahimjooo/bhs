/*
 Navicat Premium Data Transfer

 Source Server         : DV LOC
 Source Server Type    : MySQL
 Source Server Version : 50621
 Source Host           : localhost:3306
 Source Schema         : behaustex

 Target Server Type    : MySQL
 Target Server Version : 50621
 File Encoding         : 65001

 Date: 11/07/2021 20:49:38
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for employee
-- ----------------------------
DROP TABLE IF EXISTS `employee`;
CREATE TABLE `employee`  (
  `EmployeeID` int(11) NOT NULL AUTO_INCREMENT,
  `Username` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `Description` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `Rank` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `Password` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `BirthPlace` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `BirthDate` date NULL DEFAULT NULL,
  `Sallary` double(255, 0) NULL DEFAULT NULL,
  `Tunjangan` double(255, 0) NULL DEFAULT NULL,
  `Bpjs` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`EmployeeID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of employee
-- ----------------------------
INSERT INTO `employee` VALUES (1, 'malik', 'Malik Ibrahim', 'IT Programmer', '123', 'New Zeland', '2021-07-11', 5200000, 170000, 'Ya');
INSERT INTO `employee` VALUES (2, 'joni', 'Eudora Ratu Petir', 'IT Programmer', '123', 'Jakarta', '2021-07-11', 4769000, 246000, 'Ya');
INSERT INTO `employee` VALUES (3, 'balmon', 'Balmon Selalu Ceria', 'IT Programmer', '123', 'Matraman', '2021-07-11', 4590000, 156000, 'Tidak');
INSERT INTO `employee` VALUES (4, NULL, 'Chelsea Island', 'IT Programmer', NULL, 'Jogjakarta', '2021-07-11', 5210000, 329000, 'Ya');

-- ----------------------------
-- Table structure for overtime
-- ----------------------------
DROP TABLE IF EXISTS `overtime`;
CREATE TABLE `overtime`  (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `EmployeeID` int(11) NULL DEFAULT NULL,
  `TransDate` date NULL DEFAULT NULL,
  `Overtime` double NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of overtime
-- ----------------------------
INSERT INTO `overtime` VALUES (1, 1, '2021-07-13', 4);
INSERT INTO `overtime` VALUES (2, 3, '2021-07-22', 10);

-- ----------------------------
-- Table structure for sallaryrecap
-- ----------------------------
DROP TABLE IF EXISTS `sallaryrecap`;
CREATE TABLE `sallaryrecap`  (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `EmployeeID` int(11) NULL DEFAULT NULL,
  `Bulan` int(11) NULL DEFAULT NULL,
  `Kerja` double(20, 2) NULL DEFAULT NULL,
  `Lembur` double(20, 2) NULL DEFAULT NULL,
  `Bpjs` double(20, 2) NULL DEFAULT NULL,
  `Gaji` double(20, 2) NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for userlogin
-- ----------------------------
DROP TABLE IF EXISTS `userlogin`;
CREATE TABLE `userlogin`  (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Username` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `role` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of userlogin
-- ----------------------------
INSERT INTO `userlogin` VALUES (1, 'admin', '202cb962ac59075b964b07152d234b70', 'Staf');
INSERT INTO `userlogin` VALUES (2, 'superadmin', '202cb962ac59075b964b07152d234b70', 'Supervisor');

-- ----------------------------
-- Table structure for workabsent
-- ----------------------------
DROP TABLE IF EXISTS `workabsent`;
CREATE TABLE `workabsent`  (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `EmployeeID` int(11) NULL DEFAULT NULL,
  `TransDate` date NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of workabsent
-- ----------------------------
INSERT INTO `workabsent` VALUES (1, 1, '2021-07-12');
INSERT INTO `workabsent` VALUES (2, 1, '2021-07-21');
INSERT INTO `workabsent` VALUES (3, 2, '2021-07-22');

SET FOREIGN_KEY_CHECKS = 1;
